#!/bin/sh
DATA_FOLDER=~/Documents/workout
MAXES_FILE=$DATA_FOLDER/_maxes.tsv
TODAY_WORKOUT=$DATA_FOLDER/$(date +'%Y-%m-%d').tsv

SMALLEST_WEIGHT="5"

getTrainingMax() {
    excersize=$1
    echo "$(grep $excersize $MAXES_FILE | cut -f2) * 0.9" | bc
}

getWeekNum() {
    echo $((($(ls $DATA_FOLDER | wc -l) - 1) / 4))
}

supplementWeights() {
    TRAINING_MAX=$(getTrainingMax $1)
    PERCENTS=$(yes 0.5 | head -n 5 | tr '\r\n' ' ')

    echo "$(GetWeights "$PERCENTS" $1)"
}

GetWeights() {

    TRAINING_MAX=$(getTrainingMax $2)
    PERCENTS=$1
    WEIGHTS=""

    for PERCENT in $PERCENTS
    do
        # https://stackoverflow.com/questions/2395284/round-a-divided-number-in-bash
        prop=$(echo "(($PERCENT * $TRAINING_MAX + ($SMALLEST_WEIGHT / 2))/ $SMALLEST_WEIGHT) " | bc)
        curWeight=$(echo "$prop * $SMALLEST_WEIGHT" | bc)
        WEIGHTS="$WEIGHTS $curWeight"
    done

    echo $WEIGHTS
}

WorkoutWeights() {
    WARMUP_PERCENT="0.4 0.5 0.6"

    WEEK=$(getWeekNum)

    case $WEEK in
        0)
            WORKOUT_PERCENT="0.65 0.75 0.85";;
        1)
            WORKOUT_PERCENT="0.70 0.80 0.90";;
        2)
            WORKOUT_PERCENT="0.75 0.85 0.95";;
        3)
            WORKOUT_PERCENT="0.40 0.50 0.60";;
        *)
            echo "update your maxes" && exit 1;;
    esac

    PERCENTS="$WARMUP_PERCENT $WORKOUT_PERCENT"

    echo $(GetWeights "$PERCENTS" $1)
}

gui() {
    EXCERSIZE="$1"
    REPS="$2"
    WEIGHTS="$3"

    idx="$(seq -s ' ' 1 $(echo $WEIGHTS | wc -w))"

    for i in $idx;
    do
	    EXCER_NAME=$EXCERSIZE-$(echo "$WEIGHTS" | cut -d' ' -f$i)
	    OPTIONS="$(seq $(echo "$REPS" | cut -d' ' -f$i) -1 0)\ncancel"
	    REPS_DONE=$(echo -e "$OPTIONS" | dmenu -p $EXCER_NAME -l 10 -fn "Terminus-30" -wm)
	    [ $REPS_DONE = "cancel" ] && exit 0
	    echo -e "$EXCER_NAME\t$REPS_DONE" >> $TODAY_WORKOUT
	    sleep 1m
    done

}


    PRIMARY="overhead deadlift bench squat"
    SUPPLEMENT="bench squat overhead deadlift"

    ROUTINE_NUM=$(($(($(ls $DATA_FOLDER | wc -l) + 1)) % $(echo $PRIMARY | wc -w)))

    # TODO: Must increase weight automatically
    [ $(getWeekNum) -eq 3 ] && [ $ROUTINE_NUM -eq 4 ] && exit 1

    PRIMARY_EXCERSIZE=$(echo $PRIMARY | cut -d " " -f $ROUTINE_NUM)
    SUPPLEMENT_EXCERSIZE=$(echo $SUPPLEMENT | cut -d " " -f $ROUTINE_NUM)
    echo "$PRIMARY_EXCERSIZE"

    WEIGHTS=$(WorkoutWeights $PRIMARY_EXCERSIZE)
    REPS="5 5 3 5 5 5"

    SUP_REPS="10 10 10 10 10"
    SUP_WEIGHTS=$(supplementWeights $SUPPLEMENT_EXCERSIZE)
    echo "$REPS"
    echo "$PRIMARY_EXCERSIZE"
    echo "$WEIGHTS"

    gui "$PRIMARY_EXCERSIZE" "$REPS" "$WEIGHTS"
    gui "$SUPPLEMENT_EXCERSIZE" "$SUP_REPS" "$SUP_WEIGHTS"
