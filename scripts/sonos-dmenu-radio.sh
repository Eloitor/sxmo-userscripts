#!/bin/sh -x
dmenucmd="sxmo_dmenu.sh -p >>>>>> -i -l 10"
uri=$(sonos-pjh.py listradio | sort -u | $dmenucmd)
[ -z "$uri" ] && exit
full_uri="$(echo "$uri" | cut -d'*' -f2 | sed -e 's/^[[:space:]]*//')"
sonos-pjh.py clearq
sonos-pjh.py loadradio "$full_uri"
sonos-pjh.py play


