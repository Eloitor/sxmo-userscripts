#!/usr/bin/env sh

# Lists the various IP addresses in a menu.

# The menu will show both IPv4 and IPv6 (if applicable) for every interface
# on the device (not just wlan0). Selecting an IP address will copy it to the keyboard

# The MIT License (MIT)
#
# Copyright (c) 2021 Nathaniel Barragan
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

xclip() {
	case "$SXMO_WM" in
		sway)
			wl-copy
			;;
		*)
			xclip
			 ;;
  esac
}

getips() {
	for iface in $(nmcli -t connection show --active | awk -F ':' '{print $4}'); do
		echo "Interface: $iface"
		ip addr show "$iface" | awk ' /inet/ {print $2}'
	done
}

listipmenu() {
	SELECTION="$(
		printf %b "$(getips)\nClose Menu" | sxmo_dmenu.sh -c -p 'IP' -l 14
	)"
	case "$SELECTION" in
		"Close Menu" )
			;;
		Interface:* )
			echo "$SELECTION" | sed 's/Interface: //' | xclip
			notify-send "Copied interface name to clipboard"
			;;
		* )
			echo "$SELECTION" | xclip
			notify-send "Copied IP address to clipboard"
			;;
	esac
}

if [ $# -gt 0 ]; then
	"$@"
else
	listipmenu
fi






