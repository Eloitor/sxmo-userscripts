#! /bin/sh

# To use: edit $ALARM_FILE variable
# Use cron to set your alarm!
# ie: 0 12 * * WED sh ~/.config/sxmo/userscripts/alarm.sh 

ALARM_FILE="/home/anjan/Documents/alarm.opus"
pulsemixer --id sink-42 --unmute --set-volume 100

mpv --audio-device=alsa/sysdefault:CARD=PinePhone Documents/alarm.opus --no-resume-playback --loop-file=inf --vid=no "$ALARM_FILE"
