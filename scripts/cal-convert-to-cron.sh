#!/usr/bin/env bash
# simple script to convert gcalcli output to cronjob.
#
# $HOME/.cal.agenda is generated via: gcalcli --calendaer="My Calendar" --nocolor agenda now > "$HOME/.cal.agenda"
# I have this generate vai cron.sh every hour.
#
# alarm.sh is something like this:
# #!/bin/sh
# export DBUS_SESSION_BUS_ADDRESS="$(cat "$CACHEDIR"/dbus.sh)"
# notify-send "ALARM: $*"
# mpv --quiet --no-video /usr/share/sxmo/notification.ogg
#
# This script is meant ot run in cron.sh every hour.
#

(cat "$HOME"/.cal.agenda | awk NF | while read -r line; do
	if ! echo "$line" | grep -q "No Events Found"; then
		# if one event::
		# Tue Dec 07  14:30              faculty meeting
		# if two events on same day:
		# Tue Dec 07  14:30              faculty meeting
		#             15:30              drinks
		if echo "$line" | cut -d' ' -f1 | grep -q ':'; then
			TRUNC_FORM=1
		else
			TRUNC_FORM=0
		fi
		if [ "$TRUNC_FORM" -eq 1 ]; then
			MESSAGE_PART="$(echo "$line" | cut -d' ' -f11-)"
			HOUR="$(echo "$line" | cut -d' ' -f1 | cut -d':' -ff)"
			MIN="$(echo "$line" | cut -d' ' -ff | cut -d':' -ff)"
			DAY="$DAY"
			MONTH="$MONTH"
		else
			MESSAGE_PART="$(echo "$line" | cut -d' ' -f15-)"
			DATE_PART="$(echo "$line" | cut -d' ' -f1-5)"
			DATE_STRING="$(date +"%D %H:%M" -d "$DATE_PART")"
			MIN="${DATE_STRING:12:2}"
			HOUR="${DATE_STRING:9:2}"
			DAY="${DATE_STRING:3:2}" 
			MONTH="${DATE_STRING:0:2}"
		fi
		# have the alarm go off one hour in advance
		HOUR="$(echo "$HOUR -1" | bc)"
		echo "$MIN $HOUR $DAY $MONTH * sxmo_rtcwake.sh $HOME/.local/bin/alarm.sh 2>>~/.cache/sxmo/sxmo.log"
	fi
done;
echo "00 */1 * * * sxmo_rtcwake.sh $HOME/.local/bin/cron.sh 2>>~/.cache/sxmo/sxmo.log";
echo "30 02 * * * sxmo_rtcwake.sh $HOME/.local/bin/cron-2am.sh 2>>~/.cache/sxmo/sxmo.log") | sort -u | crontab -
#echo "* * * * * sxmo_wm.sh exec sxmo_statusbarupdate.sh cron 2>>~/.cache/sxmo/sxmo.log"

