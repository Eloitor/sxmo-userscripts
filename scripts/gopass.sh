#!/bin/sh

# requires: gopass, xclip, and pinentry-gtk installed
# add the line `pinentry-program /usr/bin/pinentry-gtk-2` to ~/.gnupg/gpg-agent.conf

gopass ls --flat | sxmo_dmenu_with_kb.sh -l 10 -fn "Terminus-25" -c | xargs --no-run-if-empty gopass show -c
