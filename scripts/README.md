# User hooks

This directory contains example userscripts contributed by the community.

# Usage

Read the comments in the script you want to use.
Copy the script to `$XDG_CONFIG_HOME/sxmo/userscripts` and make it executable.

# Summary of scripts

## sonos-dmenu.sh
- Author: Peter <peterjohnhartman@gmail.com>
- License: MIT
- Description: sonos demnu script

## cal-convert-to-cron.sh
- Author: Peter <peterjohnhartman@gmail.com>
- License: MIT
- Description: converts gcalcli to crontab

## onepassword.py
- Author: Kim Visscher <kim@visscher.codes>
- License: MIT
- Description: A mobile friendly interface for [1Password](https://1password.com/)

## Workout
- Author: Anjandev Momi <anjan@momi.ca>
- License: MIT
- Description: A mobile workout app for Jim Wendler's 531 big but boring written with unix coreutils and Sxmo's dmenu fork.

## gopass
- Author: Anjandev Momi <anjan@momi.ca>
- License: MIT
- Description: A mobile interface for [gopass](https://www.gopass.pw/)

## alarm
- Author: Anjandev Momi <anjan@momi.ca>
- License: MIT
- Description: Set an alarm on the pinephone with cron

## appstore.sh
- Author: Zach DeCook <zachdecook@librem.one>
- License: MIT
- Description: Download recommended apps for sxmo

## mail.sh
- Author: Anjandev Momi <anjan@momi.ca>
- License: MIT
- Description: Use mblaze, dmenu, and msmtp to see emails

## musicpd.sh
- Author: Stacy Harper <contact@stacyharper.net>
- License: MIT
- Description: Dmenu mpd client to control music

## ticker-portfolio
- Author: Anjandev Momi <anjan@momi.ca>
- License: MIT
- Description: track your stock/cryptocurrency portfolio via the cli
- Link: https://git.sr.ht/~anjan/ticker-portfolio

## my_photos.sh
- Author: None
- License: WTFPL 
- Description: Displays my photos in reverse chronological order, using feh

## getip.sh
- Author: Nathaniel Barragan <nathanielbarragan@protonmail.com>
- License: MIT
- Description: Lists the various IP addresses in a menu.

## vcf2tsv.py
- Author: Gled <gled@remote-shell.net>
- License: WTFPL
- Description: Generates a sxmo contact list from a vcard format export ( .vcf )

## scale_display.sh
- Author: Frank Oltmanns <alpine@oltmanns.dev>
- License: MIT
- Description: A simple script that let's you select the scale factor of the device's display.

## sxmo_invidious.sh
- Author: Stacy Harper <contact@stacyharper.net>
- License: GPL3+
- Description: A complete script to search videos and playlists and to follow channels new videos.
