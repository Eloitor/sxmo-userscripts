#!/bin/sh
dmenucmd="sxmo_dmenu_with_kb.sh -p >>>>>> -i -l 10"
search_type="$(printf "tracks\ngenres\nalbums\nartists" | $dmenucmd)"
[ -z "$search_type" ] && exit # exit if empty search
search_value="$(echo "" | $dmenucmd)"
[ -z "$search_value" ] && exit # exit if empty search
echo "st: $search_type sv: $search_value"
sonos-pjh.py clearq
sonos-pjh.py search "$search_type" "$search_value" | $dmenucmd
sonos-pjh.py play

