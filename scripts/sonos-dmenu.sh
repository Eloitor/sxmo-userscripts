#!/bin/sh
function myinfo() {
	if [ -n "$DISPLAY" ]; then 
		notify-send "$*"
	elif [ -n "$WAYLAND_DISPLAY" ]; then
		notify-send "$*"
	else
		printf %s "$*"
	fi
}

dmenucmd="sxmo_dmenu.sh -p >>>>>> -i -l 10"


function mainloop() {

	res=$(printf "Cancel\nPause\nPlay\nVol Up\nVol Down\nPick Artist-Album\nPick Radio Station\nSearch\nCurrent\nNext\nPrev\nRall\nShuffle\nNormal\nRepeat All\nUpdate\nPrint Q\nClear Q\n" | $dmenucmd)
	[ -z "$res" ] && exit

	case "$res" in
		"Search")
			sonos-dmenu-search.sh
			mainloop
			;;
		"Pause")
			myinfo "$(sonos-pjh.py pause)"
			mainloop
			;;
		"Play")
			myinfo "$(sonos-pjh.py play)"
			mainloop
			;;
		"Vol Up")
			myinfo "$(sonos-pjh.py vol up)"
			mainloop
			;;
		"Vol Down")
			myinfo "$(sonos-pjh.py vol down)"
			mainloop
			;;
		"Pick Artist-Album")
			sonos-dmenu-artists-albums.sh
			mainloop
			;;
		"Pick Radio Station")
			sonos-dmenu-radio.sh
			mainloop
			;;
		"Current")
			myinfo "$(sonos-pjh.py cur)"
			mainloop
			;;
		"Next")
			myinfo "$(sonos-pjh.py next)"
			mainloop
			;;
		"Prev")
			myinfo "$(sonos-pjh.py prev)"
			mainloop
			;;
		"Rall")
			myinfo "$(sonos-pjh.py rall)"
			mainloop
			;;
		"Shuffle")
			myinfo "$(sonos-pjh.py mode shuffle)"
			mainloop
			;;
		"Normal")
			myinfo "$(sonos-pjh.py mode normal)"
			mainloop
			;;
		"Repeat All")
			myinfo "$(sonos-pjh.py mode repeat_all)"
			mainloop
			;;
		"Update")
			myinfo "$(sonos-pjh.py update)"
			main loop
			;;
		"Print Q")
			myinfo "$(sonos-pjh.py printq)"
			mainloop
			;;
		"Clear Q")
			myinfo "$(sonos-pjh.py clearq)"
			mainloop
			;;
		"Cancel")
			exit 0
			;;
	esac
}

mainloop
