#!/bin/sh
# title=" Invidious"
# Author: Stacy Harper <contact@stacyharper.net>
# License: GPL3+

. sxmo_hook_icons.sh
. sxmo_common.sh

set -e

DATA_HOME="${XDG_DATA_HOME:-$HOME/.local/share}/sxmo.invidious"
[ ! -d "$DATA_HOME" ] && mkdir -p "$DATA_HOME"
VIEWED_FILE="$DATA_HOME/viewed"
[ ! -r "$VIEWED_FILE" ] && touch "$VIEWED_FILE"
CACHE_DIR="${XDG_CACHE_HOME:-$HOME/.cache}/sxmo.invidious"
[ ! -d "$CACHE_DIR" ] && mkdir -p "$CACHE_DIR"
SUBSCRIBED_LIST="$DATA_HOME/subscribed_list"
[ ! -r "$SUBSCRIBED_LIST" ] && touch "$SUBSCRIBED_LIST"
SFEED_DIR="$DATA_HOME/sfeeds"
[ ! -d "$SFEED_DIR" ] && mkdir -p "$SFEED_DIR"
SFEED_TARGET="$CACHE_DIR/sfeedrc"
[ ! -r "$SFEED_TARGET" ] && touch "$SFEED_TARGET"

SEARCH_TYPE=video

prompt() {
	sxmo_dmenu.sh -i "$@"
}

get_instances() {
	curl -sSLG "https://api.invidious.io/instances.json?sort_by=type,users"
}

find_instance_api() {
	get_instances | jq -r '.[] | .[1] | select(.api == true) | .uri'
}

find_instance_normal() {
	get_instances | jq -r '.[] | .[1] | .uri'
}

find_instance() {
	find_instance_"${1:-normal}" | grep -v '\.onion$'
}

lazy_find_instance() {
	INSTANCE_FILE="$CACHE_DIR/invidious_instances.${1:-normal}"

	if ! [ -f "$INSTANCE_FILE" ]; then
		find_instance "$@" > "$INSTANCE_FILE"
	fi

	if [ "$(stat -c %Y "$INSTANCE_FILE")" -lt "$(($(date +%s) - 3600))" ]; then
		find_instance "$@" > "$INSTANCE_FILE"
	fi

	cat "$INSTANCE_FILE"
}

SEARCH_INSTANCE="$(lazy_find_instance api | head -n1)"
PLAY_INSTANCE="$(lazy_find_instance | head -n1)"

search_results() {
	RESULTS="$(
		curl -sSLG "$SEARCH_INSTANCE/api/v1/search" \
			--data-urlencode "q=$PICKED" \
			--data-urlencode "type=$SEARCH_TYPE" \
			| jq -r ".[] | .${SEARCH_TYPE}Id + \"|\" + .title"
	)"

	CHOICES="$(cat <<EOF
$icon_ret Return
$(printf "%s\n" "$RESULTS" | cut -d'|' -f2-)
EOF
)"

	PICKED="$(printf "%s\n" "$CHOICES" | sxmo_dmenu.sh -p "Results")"

	case "$PICKED" in
		"$icon_ret Return")
			return
			;;
		*)
			PICKED="$(printf "%s\n" "$RESULTS" | grep -m1 "|$PICKED$")"
			id="$(printf %s "$PICKED" | cut -d'|' -f1)"
			case "$SEARCH_TYPE" in
				video)
					set -- "$PLAY_INSTANCE/watch?v=$id"
					;;
				playlist)
					set -- "$PLAY_INSTANCE/playlist?list=$id"
					;;
			esac

			sxmo_notify_user.sh "Starting the video !"
			mpv ${SXMO_MPV_PROFILE:+--profile="$SXMO_MPV_PROFILE"} --fs "$@"
			;;
	esac
}

search_main() {
	while : ; do
		HIST_FILE="$DATA_HOME/$SEARCH_TYPE.history"
		touch "$HIST_FILE"

		CHOICES="$(cat <<EOF
$icon_ret Return
$(
	[ "$SEARCH_TYPE" = video ] &&
		printf "Playlist %s" "$icon_tof" ||
		printf "Playlist %s" "$icon_ton"
)
$(tac "$HIST_FILE" | awk '!showed[$0] { showed[$0]=1; print $0 }')
EOF
		)"

		PICKED="$(printf "%s\n" "$CHOICES" | sxmo_dmenu.sh -p Search -I "$INDEX")" || exit
		INDEX="$(($(printf "%s" "$CHOICES" | grep -Fnm1 "$PICKED" | cut -d: -f1) -1))"

		case "$PICKED" in
			"$icon_ret Return")
				return
				;;
			"Playlist $icon_tof")
				SEARCH_TYPE=playlist
				;;
			"Playlist $icon_ton")
				SEARCH_TYPE=video
				;;
			*)
				printf "%s\n" "$PICKED" >> "$HIST_FILE"
				search_results
		esac
	done
}

subscribed() {
	while : ; do
		ENTRIES="$(
			find "$SFEED_DIR" -type f -print0 |
			xargs -0n1 sfeed_plain |
			sed 's/^N/ /' |
			sort -dr |
			awk -v VIEWED_FILE="$VIEWED_FILE" '
				FILENAME == VIEWED_FILE {
					viewed[$1]++
					next
				}
				printed[$NF] { next }
				{ $1=$2="";$0=$0;$1=$1; printed[$NF]++ }
				viewed[$NF] { printf " " }
				{ print $0 }
			' "$VIEWED_FILE" -
		)"
		CHOICES="$(cat <<EOF
$icon_cls Close Menu
$icon_fnd Search
$icon_cfg Manage subscriptions
$icon_rld Update
$(printf %b "$ENTRIES" | awk '{$NF=""; print $0}')
EOF
)"
		CHOICE="$(
			printf %b "$CHOICES" |
			awk '/./ {$1=$1; print $0};' |
			cut -d'^' -f1 |
			sxmo_dmenu.sh -i -p Subscriptions ${INDEX:+-I "$INDEX"}
		)" || exit

		case "$CHOICE" in
			"$icon_cls Close Menu")
				return
				;;
			"$icon_fnd Search")
				search_main
				;;
			"$icon_rld Update")
				persist_sfeed
				sxmo_terminal.sh sfeed_update "$SFEED_TARGET"
				INDEX=
				;;
			"$icon_cfg Manage subscriptions")
				manage_subscriptions
				;;
			*)
				INDEXLINE="$(printf %b "$ENTRIES" | grep -n -F "$CHOICE" | head -n1)"
				INDEX="$(($(printf %b "$INDEXLINE" | cut -d: -f1)+3))"
				LINE="$(printf %b "$INDEXLINE" | cut -d" " -f3-)"
				URL="$(printf %b "$LINE" | awk '{ print $NF }')"

				grep -q "^$URL$" < "$VIEWED_FILE" || printf "%s\n" "$URL" >> "$VIEWED_FILE"

				URL="$(
					printf %s "$URL" | sed \
						-e "s|https://youtu.be/|$PLAY_INSTANCE/|g" \
						-e "s|https://www.youtube.com/|$PLAY_INSTANCE/|g"
				)"

				sxmo_notify_user.sh "Starting the video !"
				mpv ${SXMO_MPV_PROFILE:+--profile="$SXMO_MPV_PROFILE"} --fs "$URL"
				;;
		esac
	done
}

list_subscriptions() {
	cut -d" " -f2- < "$SUBSCRIBED_LIST"
}

list_to_sfeedrc() {
	cat <<EOF
sfeedpath="$SFEED_DIR"

feeds() {
EOF

	while read -r url name; do
		name="$(printf %s "$name" | sed "s|\'|\'\\\\'\'|g")"
		printf "\tfeed '%s' '%s'\n" "$name" "$url"
	done < "$SUBSCRIBED_LIST"

	cat <<EOF
}
EOF
}

persist_sfeed() {
	list_to_sfeedrc > "$SFEED_TARGET"
}

confirm() {
	printf "No\nYes\n" | prompt -p "$*" | grep -q '^Yes$'
}

#probe_url() {
#	if ! result="$(
#		yt-dlp -J "$1" --playlist-items 0 |
#		jq -r '.channel_id + "\n" + .channel'
#	)" || [ -z "$result" ] ; then
#		sxmo_notify_user.sh "yt-dlp failed !"
#		return
#	fi
#	channel_id="$(printf "%s\n" "$result" | head -n1)"
#	channel_name="$(printf "%s\n" "$result" | tail -n1)"
#	rss_url="https://www.youtube.com/feeds/videos.xml?channel_id=$channel_id"
#
#	printf '%s %s' "$rss_url" "$channel_name"
#}

search_channel_rss() {
	PICKED="$(printf "" | prompt)"

	CHOICES="$(
		curl -sSLG "$SEARCH_INSTANCE/api/v1/search" \
			--data-urlencode "type=channel" \
			--data-urlencode "q=$PICKED" \
			| jq -r '.[] | .authorId + " " + .author'
	)"

	PICKED="$(
		printf "%s\n" "$CHOICES" \
			| cut -d" " -f2- \
			| prompt -p "Search channel"
	)"

	PICKED="$(printf "%s\n" "$CHOICES" | grep -m1 "[^ ]\+ $PICKED$")"

	channel_id="$(printf %s "$PICKED" | cut -d" " -f1)"
	channel_name="$(printf %s "$PICKED" | cut -d" " -f2-)"

	printf 'https://www.youtube.com/feeds/videos.xml?channel_id=%s %s' "$channel_id" "$channel_name"
}

new_subscription() {
	CHOICES="$(cat <<EOF
$icon_ret Return
$icon_fnd Search
$icon_cpy Paste
EOF
	)"

	PICKED="$(printf "%s\n" "$CHOICES" | prompt -p "Subscribed")"

	case "$PICKED" in
		"$icon_ret Return")
			return
			;;
		"$icon_fnd Search")
			line="$(search_channel_rss)"
			;;
		#"$icon_cpy Paste")
		#	query="$(wl-paste)"
		#	line="$(probe_url "$query")"
		#	;;
	esac

	[ -z "$line" ] && return

	rss_url="$(printf "%s" "$line" | cut -d" " -f1)"
	channel_name="$(printf "%s" "$line" | cut -d" " -f2-)"

	if grep -q "^$rss_url " "$SUBSCRIBED_LIST"; then
		sxmo_notify_user.sh "Already subscribed !"
		return
	fi

	if confirm "Sub to \"$channel_name\" ?"; then
		printf "%s\n" "$line" >> "$SUBSCRIBED_LIST"
		sxmo_notify_user.sh "Subscribed !"
	fi
}

manage_subscriptions() {
	while : ; do
		CHOICES="$(cat <<EOF
$icon_ret Return
$icon_pls New sub
$(list_subscriptions)
EOF
		)"

		PICKED="$(printf "%s\n" "$CHOICES" | prompt -p "Manage subs")" || exit

		case "$PICKED" in
			"$icon_ret Return")
				return
				;;
			"$icon_pls New sub")
				new_subscription
				;;
			*)
				if confirm "Unsub to \"$PICKED\" ?"; then
					sed -i "/[^ ]\+ $PICKED$/d" "$SUBSCRIBED_LIST"
					sxmo_notify_user.sh "Unsubscribed !"
				fi
		esac
	done
}

subscribed
