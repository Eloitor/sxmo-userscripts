#!/bin/sh
rm -f ~/.wallpaper.jpg
url=$(sonos-pjh.py cur | grep "^Album Art:" | cut -d':' -f2-)
echo "url: $url"
wget $url -O ~/.wallpaper.jpg
feh --bg-fill ~/.wallpaper.jpg
