#!/usr/bin/env sh

# minimal email client.
# script requires mbsync (isync) and mu
# mbsync is required to send email
# If you have alot of email use
# format of MAILDIR should be $MAILDIR/ACCOUNT/{INBOX,Archive,etc.}/{cur,new,tmp}


findaccount() {
	MAILNUM="$1"
	MAILPATH="$(mseq -r "$MAILNUM")"
	REL="$(realpath --relative-to "$MAILDIR" "$MAILPATH")"
	ACCOUNT="$(echo "$REL" | cut -f1 -d '/')"
	echo "$ACCOUNT"
}

finddir() {
	ACCOUNT="$1"

	FINDME="$2"

	# Must be defined by user
	case "$FINDME" in
		"inbox")
			[ -d "$MAILDIR"/"$ACCOUNT"/"Inbox" ] && echo "Inbox"
			[ -d "$MAILDIR"/"$ACCOUNT"/"INBOX" ] && echo "INBOX"
			;;
		"archive")
			[ -d "$MAILDIR"/"$ACCOUNT"/"Archive" ] && echo "Archive"
			;;
		"sent")
			[ -d "$MAILDIR"/"$ACCOUNT"/"Sent" ] && echo "Sent"
			[ -d "$MAILDIR"/"$ACCOUNT"/"Sent Items" ] && echo "Sent Items"
			;;
	esac

}

prompt() {
    sxmo_dmenu.sh -i "$@"
}

attachmenthandler() {
	ATTACH="$1"
	MIME="$(echo "$ATTACH" | cut -f 2 -d ":" | cut -f 2 -d " ")"
	ATTACHNUM="$(echo "$ATTACH" | cut -f1 -d':')"

	tmp="$(mktemp '/tmp/mblaze.XXXXXX')"
	mshow -O "$MAIL" "$ATTACHNUM" > "$tmp"
	# replace this with xdg?
	case "$MIME" in
		"application/pdf")
			zathura "$tmp"
			;;
		"text/html")
			firefox "$tmp"
			;;
	esac
	rm "$tmp"

}

showmsg() {
	incontext="$1"

	if [ $incontext = 1 ]
	then
		sxmo_terminal.sh mless .
	else
		sxmo_terminal.sh mless
	fi

	while : ;
	do
		CHOICE="$(printf %b "attachments\ncontext\nopen url\nrefile\nCancel" | prompt )"
		case "$CHOICE" in
			"attachments")
				ATTACH="$(mshow -t . | prompt )"
				attachmenthandler "$ATTACH"
				;;
			"context")
				[ $incontext = 1 ] && showmsg 1 && return
				ACCOUNT="$(findaccount .)"
				SENTDIR="$(finddir "$ACCOUNT" "sent")"
				ARCHIVEDIR="$(finddir "$ACCOUNT" "archive")"
				INBOXDIR="$(finddir "$ACCOUNT" "inbox")"
				mthread -S "$MAILDIR/$ACCOUNT/$SENTDIR" -S "$MAILDIR/$ACCOUNT/$ARCHIVEDIR" -S "$MAILDIR/$ACCOUNT/$INBOXDIR" . | mseq -S
				showmsg 1
				break ;;
			"open url")
				which urlview || echo 'Please install urlview' | prompt
				mshow -O . | grep -Eo "(http|https)://[a-zA-Z0-9./?=_%:-]*" | sort -u | prompt | xargs -r firefox
				;;
			"refile")
				ACCOUNT="$(findaccount .)"
				ARCHIVEDIR="$(finddir "$ACCOUNT" "archive")"
				mrefile -v . "$MAILDIR/$ACCOUNT/$ARCHIVEDIR" || echo "error occured" | prompt
				sxmo_terminal.sh mless .+1
				;;
			*)
				break ;;
		esac

	done

}

mainmenu() {
	account="$(ls -1 "$MAILDIR" | prompt)"
	while : ;
	do
		CHOICE="$(printf %b "Get Mail\nChange Account\nShow Inbox\nShow Archive\nCancel" | prompt -p "$account")"
		case "$CHOICE" in
			"Change Account")
				account="$(ls -1 "$MAILDIR" | prompt)"
				;;
			"Get Mail")
				sxmo_terminal.sh mbsync -a -c "$MBSYNC_CONFIG"
				;;
			"Show Inbox")
				#mlist "$MAILDIR"/"$account"/"$(finddir "$account" "inbox")" | mthread -r | mseq -S
				mlist "$MAILDIR"/"$account"/"$(finddir "$account" "inbox")" | msort -dr | mseq -S
				showmsg 0
				;;
			"Show Archive")
				#mlist "$MAILDIR"/"$account"/"$(finddir "$account" "archive")" | mthread -r | mseq -S
				mlist "$MAILDIR"/"$account"/"$(finddir "$account" "archive")" | msort -dr | mseq -S
				showmsg 0
				;;
			*)
				kill $$;;
		esac
	done

}

# todo, move to startup script and check if set by user
export MAILDIR=~/.local/share/mail
export MBSYNC_CONFIG=~/.config/isync/mbsyncrc
export MBLAZE="$XDG_CONFIG_HOME/mblaze"
export MBLAZE_PAGER="less -R"

which mbsync || echo 'Please install isync' | prompt

[ -f "$MBSYNC_CONFIG" ] || echo 'Please configure isync' | prompt

mkdir -p "$MBLAZE"
mainmenu
