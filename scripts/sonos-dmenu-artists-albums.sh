#!/bin/sh -x
dmenucmd="sxmo_dmenu_with_kb.sh -p >>>>>> -i -l 10"
#to repopulate the allartists database: rm -f ~/.sonos-allartists
[ -f ~/.sonos-allartists ] || sonos-pjh.py printallartists | sort -u > ~/.sonos-allartists
artist="$($dmenucmd < ~/.sonos-allartists)"
[ -z "$artist" ] && exit # exit if empty album, i.e., they canceled
album="$(sonos-pjh.py printalbumsfromartist "$artist" | sort -u | $dmenucmd)"
[ -z "$album" ] && exit # exit if empty album, i.e., they canceled
sonos-pjh.py unradio
sonos-pjh.py clearq
sonos-pjh.py search albums "$album"
sonos-pjh.py play | $dmenucmd
