#!/bin/sh
# Author: Zach DeCook <zachdecook@librem.one>
# License: MIT

## Data

ID_LIKE="$(sh -c '. /etc/os-release; echo "${ID_LIKE:-$ID}"')"

# Data comes from the apps hook.
# Outputs $icon_name\tbin_name
listApps(){
	grep ^write_line_app /usr/share/sxmo/default_hooks/*apps* | tr -d '"'| awk '{print $3 "\t" $2}'
}
# Categories should have 4-12 apps.
# These are the menu categories, with the icons which go with them.
Menus='Messaging	\(msg\|tgm\)
Browsing	\(ffx\|glb\)
Email	eml
Editing	\(edt\|vim\)
Multimedia	\(cam\|mic\|mus\|mvi\)
Files	dir
Terminal	trm
Maps	\(gps\|map\)
Misc	\(and\|bok\|cfg\|chs\|clc\|clk\|img\|inf\|itm\|red\|rld\|rss\)
exit'

## Functions

pkgInfo(){
	pkg="$1"
	case "$ID_LIKE" in
		"alpine")
			apk info -dws "$pkg" | grep -v '^$' | grep -v ' webpage:' | grep -v 'installed size:'
			;;
		"debian")
			# apt has an unstable cli
			# lines starting with a space are additional description lines
			apt info "$pkg" | grep '\(Package\|Version\|Description\| \|\)' | sed 's/: /\t/g' | cut -f2
			;;
		"arch")
			# -Si means remote information
			pacman -Si "$pkg" | grep '^\(Name\|Version\|Description\|URL\)' | sed 's/ : /\t/g' | cut -f2
			;;
		*)
			printf "I don't know how to get package information in '%s' distros\n" "$ID_LIKE"
			;;
	esac
}
pkgInstall(){
	pkg="$1"
	case "$ID_LIKE" in
		"alpine")
			sxmo_terminal.sh doas apk add "$pkg"
			;;
		"debian")
			sxmo_terminal.sh doas apt-get install "$pkg"
			;;
		"arch")
			sxmo_terminal.sh doas pacman -Sy "$pkg"
			;;
		*)
			printf "I don't know how to install packages in '%s'.\n" "$ID_LIKE"
			;;
	esac
}
pkgRemove(){
	pkg="$1"
	case "$ID_LIKE" in
		"alpine")
			sxmo_terminal.sh doas apk del "$pkg"
			;;
		"debian")
			sxmo_terminal.sh doas apt-get remove "$pkg"
			;;
		"arch")
			sxmo_terminal.sh doas pacman -R "$pkg"
			;;
		*)
			printf "I don't know how to install packages in '%s'.\n" "$ID_LIKE"
			;;
	esac
}


# Takes in bin names and returns package names which provide those.
pkgNames(){
	case "$ID_LIKE" in
		"alpine")
			sed 's/^/cmd:/g' | xargs -r apk search -xqa | uniq
			;;
		"debian")
			if command -v apt-file; then
				#in my experience, apt-file is really slow...
				xargs -r -I{} apt-file find -Fl /usr/bin/{}
				# sed s'@^@/usr/bin/@g' | apt-file find -Flf /dev/stdin
			else
				printf "apt-file must be installed (and updated) to find packages which provide a certain bin.\n" > /dev/stderr
			fi
			;;
		"arch")
			sed 's@^@/usr/bin/@g' | xargs -r pacman -Fq | cut -d/ -f2
			;;
		*)
			printf "I don't know how to find package names from commands in '%s'.\n" "$ID_LIKE" > /dev/stderr
			;;
	esac
}


## Menus

topMenu(){
	sel="$(printf '%s' "$Menus" | cut -f1 | sxmo_dmenu.sh -i -p "App Store")"
	if ! test "$sel" || test "$sel" = "exit"; then
		exit 0
	fi
	menuName "$sel"
}

menuName(){
	name="$1"
	typ="$(echo "$Menus" | grep "$name	" | cut -f2)"
	# shellcheck disable=SC2016
	grep='^\$icon_'"$typ"'	'
	pkg="$(listApps | grep "$grep" |cut -f2|pkgNames|awk 'BEGIN{print ".."} ($0){print} END{print "exit"}'|sxmo_dmenu.sh -i -p "$name")"
	if test "$pkg" = "exit" || ! test "$pkg"; then
		exit
	elif test "$pkg" = ".."; then
		topMenu
	else
		pkgMenu "$pkg" "$name"
	fi
}

pkgMenu(){
	title="$(printf '%s' "$1"|cut -f1)"
	menu="..
install
uninstall
$(pkgInfo "$1")
exit"
	while true; do
	sel="$(printf '%s\n' "$menu"|sxmo_dmenu.sh -p "$title")"
	case "$sel" in
		"install")
			pkgInstall "$1"
			;;
		"uninstall")
			pkgRemove "$1"
			;;
		"..")
			menuName "$2"
			return
			;;
		"exit"|"")
			exit
			;;
		*"://"*)
			sxmo_open.sh "$sel"
			exit
			;;
		*)
			notify-send "$sel"
			;;
	esac
	done
}

## Execution

topMenu
