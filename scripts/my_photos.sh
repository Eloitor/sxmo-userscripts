#/bin/ash
#
# Pretty straightforward way of displaying all my photos, in invert chronological order, in feh
#
/usr/bin/feh  --cache-size 150 -t --index-info '' -y 180 -E 180 -P -d -J 1 --edit -s -B black -. -S mtime /home/user/Pictures/*jpg
