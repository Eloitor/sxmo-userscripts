# SXMO user scripts

A community repository of userscripts to use with sxmo.

# Contributing

Patches are welcome.
Add a summary of your script to the `README.md` file and script itself to the correct folder (hooks or scripts).
Please use [git send-email](https://git-send-email.io/) and send a patch to my [public inbox](https://lists.sr.ht/~anjan/public-inbox).
