Place 'sms' in your hooks directory and the various sxmo* files into PATH.

This hook does three things, and you can remark out the parts you don't wnat in
the sms file.

(1) detects a specific message that will wake up the phone.  Note: in order for
this to work, you will need to add 'sms' to can_suspend so that the phone won't
go back into crust must while the sms script is running.

(2) send sms as email.  Note: this relies on sxmo_msmtp-*queue.sh scripts.  You
should edit sxmo_msmtp-enqueue.sh.

(3) send sms to a remote weechat instance.  Note: this relies on
sxmo_sms-*queue.sh scripts.  You should edit sxmo_sms-enqueue.sh.

