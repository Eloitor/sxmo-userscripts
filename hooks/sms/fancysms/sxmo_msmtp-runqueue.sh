#!/usr/bin/env sh

QUEUEDIR="$HOME/.msmtpqueue"
LOCKFILE="$QUEUEDIR/.lock"
MAXWAIT=120

OPTIONS="$*"

stderr() {
	printf "%s sxmo_msmtp-runqueue.sh: %s.\n" "$(date +%H:%M:%S)" "$*" >&2
}

if ! ping -q -c 1 -W 10 8.8.8.8 >/dev/null 2>/dev/null; then
	stderr "Could not ping. Bailing."
	exit 1
fi

# eat some options that would cause msmtp to return 0 without sendmail mail
case "$OPTIONS" in 
	*--help*)
		stderr "Send mails in $QUEUEDIR."
		exit 0
		;;
	*--version*)
		stderr "Version 1.0."
		exit 0
		;;
esac

# wait for a lock that another instance has set
WAIT=0
while [ -e "$LOCKFILE" ] && [ "$WAIT" -lt "$MAXWAIT" ]; do
	sleep 1
	WAIT="$((WAIT + 1))"
done
if [ -e "$LOCKFILE" ]; then
	stderr "$QUEUEDIR locked." 
	exit 1
fi

# change into $QUEUEDIR 
cd "$QUEUEDIR" || exit 1

# check for empty queuedir
if [ "$(echo ./*.mail)" = './*.mail' ]; then
	stderr "No mails in $QUEUEDIR." 
	exit 0
fi

# lock the $QUEUEDIR
touch "$LOCKFILE" || exit 1

# process all mails
for MAILFILE in *.mail; do
	MSMTPFILE="$(echo $MAILFILE | sed -e 's/mail/msmtp/')"
	stderr "Sending $MAILFILE to $(sed -e 's/^.*-- \(.*$\)/\1/' $MSMTPFILE) ..."
	if [ ! -f "$MSMTPFILE" ]; then
		stderr "No corresponding file $MSMTPFILE found."
		continue
	fi
	#msmtp $OPTIONS $(cat "$MSMTPFILE") < "$MAILFILE"
	mutt "$OPTIONS" $(cat "$MSMTPFILE") < "$MAILFILE"
	if [ $? -eq 0 ]; then
		rm "$MAILFILE" "$MSMTPFILE"
		stderr "$MAILFILE sent successfully"
	else
		stderr "$MAILFILE did not send successfully."
	fi
done

# remove the lock
rm -f "$LOCKFILE"

exit 0
