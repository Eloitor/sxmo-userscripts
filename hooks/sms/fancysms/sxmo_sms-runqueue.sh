#!/usr/bin/env sh

# TODO: move to sxmo_commons.sh
QUEUEDIR="$HOME/.smsqueue"
IRC_SERVER="trillap"
WEECHAT_FIFO="~/.weechat/weechat_fifo" # on the server

LOCKFILE="$QUEUEDIR/.lock"
MAXWAIT=120

stderr() {
	printf "%s sxmo_sms-runqueue.sh: %s.\n" "$(date +%H:%M:%S)" "$*" >&2
}

OPTIONS="$*"

if ! ping -q -c 1 -W 10 8.8.8.8 >/dev/null 2>/dev/null; then
	exit 1
fi

case "$OPTIONS" in 
	*--help*)
		stderr "Send sms messages in $QUEUEDIR."
		exit 0
		;;
	*--version*)
		echo "Version 1.0."
		exit 0
		;;
esac

# wait for a lock that another instance has set
WAIT=0
while [ -e "$LOCKFILE" ] && [ "$WAIT" -lt "$MAXWAIT" ]; do
	sleep 1
	WAIT="$((WAIT + 1))"
done
if [ -e "$LOCKFILE" ]; then
	stderr "$QUEUEDIR locked."
	exit 1
fi

# change into $QUEUEDIR 
cd "$QUEUEDIR" || exit 1

# check for empty queuedir
if [ "$(echo ./*.sms)" = './*.sms' ]; then
	stderr "No sms messages in $QUEUEDIR."
	exit 0
fi

# lock the $QUEUEDIR
touch "$LOCKFILE" || exit 1

# process all mails
for SMSFILE in *.sms; do
	CHANNEL="$(cat "$SMSFILE" | cut -d' ' -f1)"
	MESSAGE="$(cat "$SMSFILE" | cut -d' ' -f2-)"
	stderr "Sending $SMSFILE to $CHANNEL."
	echo "irc.server.im */join #$CHANNEL" | ssh "$IRC_SERVER" -T "cat > $WEECHAT_FIFO"
	sleep 1
	echo "irc.im.#$CHANNEL *$MESSAGE" | ssh "$IRC_SERVER" -T "cat > $WEECHAT_FIFO"

	if [ $? -eq 0 ]; then
		rm "$SMSFILE"
		stderr "$SMSFILE sent successfully."
	else
		stderr "$SMSFILE failed to send!"
	fi
done

# remove the lock
rm -f "$LOCKFILE"

exit 0
