#!/usr/bin/env sh

# TODO: move to sxmo_common.sh
QUEUEDIR="$HOME/.smsqueue"

for i in "$QUEUEDIR"/*.sms; do
	cat "$i" 2>/dev/null || echo "No sms in queue." >&2
	echo " " >&2
done
