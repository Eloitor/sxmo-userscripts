#!/usr/bin/env sh
QUEUEDIR="$HOME/.msmtpqueue"

for i in "$QUEUEDIR"/*.mail; do
	grep -E -s -h '(^From:|^To:|^Subject:)' "$i" || echo "No mail in queue." >&2;
	echo " " 2>&1
done
