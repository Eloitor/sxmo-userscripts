#!/usr/bin/env sh

# TODO: move to sxmo_common.sh
QUEUEDIR="$HOME/.smsqueue"

# Set secure permissions on created directories and files
umask 077

# Change to queue directory (create it if necessary)
if [ ! -d "$QUEUEDIR" ]; then
	mkdir -p "$QUEUEDIR" || exit 1
fi
cd "$QUEUEDIR" || exit 1

# Create new unique filenames of the form
# SMSFILE:  ccyy-mm-dd-hh.mm.ss[-x].sms
# where x is a consecutive number only appended if you send more than one
# sms per second.
BASE="$(date +%Y-%m-%d-%H.%M.%S)"
if [ -f "$BASE.sms" ]; then
	TMP="$BASE"
	i=1
	while [ -f "$TMP-$i.sms" ]; do
		i=$((i + 1))
	done
	BASE="$BASE-$i"
fi
SMSFILE="$BASE.sms"

# Write the mail to $MAILFILE
cat > "$SMSFILE" || exit 1

exit 0
