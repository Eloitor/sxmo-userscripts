# User hooks

This directory contains example hooks contributed by the community.

# Usage

To use one of these hooks, copy it to `$XDG_CONFIG_HOME/sxmo/hooks/[name]`,
where `[name]` is the name of the directory containing the file, and make it
executable.

For example:

```
$ cp inputhandler/android_buttons $XDG_CONFIG_HOME/sxmo/hooks/inputhandler
$ chmod u+x $XDG_CONFIG_HOME/sxmo/hooks/inputhandler
```

# Contributing

Add your own hooks under a directory corresponding to the hook name and rename
the file itself something descriptive. This way, we can have multiple examples
of each type of hook organized like so:

```
hooks/
    inputhandler/
        README.md
        android_buttons
        inverted_volume
	...
```

Please also add a brief description of your hook to the README in its parent
directory.

