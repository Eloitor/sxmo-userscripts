# inputhandler hooks

This directory contains example inputhandler hooks.

# Caveats

As of version 1.5.1, this hook is only executed when the screen is unlocked.
It's not possible to modify the behavior of the power button when locked.

# Contents

## android_buttons
- Author: David Kennedy
- License: WTFPL
- Description: Modifies the hardware button behavior to mimic a typical Android
  device.

