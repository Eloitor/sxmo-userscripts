#!/bin/sh

# This hook modifies the hardware button behavior to mimic a typical Android
# device, i.e.:
#
# - The power button toggles the display on or off
# - The volume up button turns the volume up
# - The volume down button turns the volume down
#
# In addition:
#
# - Holding the volume down button mutes the volume

WMCLASS="$1"
WMNAME="$2"
ACTION="$3"

# You must exit 0 if you handled the input to not trigger default behaviors

DEBUG=0

debug_log() {
	[ $DEBUG -eq 0 ] && return
	echo "$@" >> inputhandler-hook.log
}

toggle_screenlock() {
	local action="$1"
	local state="$(sxmo_screenlock.sh getCurState)"

	# The possible values returned from getCurState are documented
	# in sxmo_screenlock.sh:
	#
	# - unlock: normal unlocked state, screen on
	# - lock: locked state with screen on
	# - off: locked state with screen off
	#
	# From my testing, "lock" appears to coincide with crust

	if [ "$state" = off -o "$state" = lock ]; then
		debug_log "action=$action, state=$state, new state=unlock"
		sxmo_screenlock.sh unlock
	else
		debug_log "action=$action, state=$state, new state=off"
		sxmo_screenlock.sh off
	fi
}

case "$ACTION" in
	powerbutton_*)
		toggle_screenlock "$ACTION"
		exit 0
		;;
	volup_*)
		sxmo_vol.sh unmute
		sxmo_vol.sh up
		exit 0
		;;
	voldown_three)
		sxmo_vol.sh mute
		exit 0
		;;
	voldown_*)
		sxmo_vol.sh down
		exit 0
		;;
esac

exit 1

